<div align="center">
  <a href="https://ambtec.com" title="ambtec" target="_blank">
    <img src="./assets/ambtec-logo-gitlab.png" width="128" />
  </a>
  <p><i>create shape improve</i></p>
</div>

<hr />

# Node Red Flow Example Service

A flow describe logical connection of endpoints, actions and outputs. It is managed as independend Node-Red project and merged together into orchestrator-flows after deployment.

### About
This flow is a simple demonstration of various http endpoints connecting to an GraphQL service.

### Notice
All changes must be merged into [main] to be pulled by [Orchestrator Flows](https://gitlab.com/ambtec/orchestrator-flows)